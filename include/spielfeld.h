#ifndef SPIELFELD_H
#define SPIELFELD_H

#include <cstdlib>
#include <iostream>

#include "zelle.h"
#include "matrix.h"

/**
 *
 */
class Spielfeld : public matrix<zelle_zustand>
{
public:
  /** Default constructor */
  Spielfeld (size_t n = 16, size_t m = 16);

  /** Default destructor */
  virtual ~Spielfeld ();

  /** Copy constructor
   *  \param other Object to copy from
   */
  Spielfeld (const Spielfeld& other);

  /** Assignment operator
   *  \param other Object to assign from
   *  \return A reference to this
   */
  Spielfeld& operator = (const Spielfeld& other);

  /**
   *
   */
  reference operator () (int64_t i, int64_t j);

  /**
   *
   */
  const_reference operator () (int64_t i, int64_t j) const;

  /**
   *
   */
  size_t anzahl_zelltypen () const;

  /**
   *
   */
  size_t nachbarn_neumann (int64_t i, int64_t j) const;

  /**
   *
   */
  size_t nachbarn_moore (int64_t i, int64_t j) const;

protected:

private:
  void index (int64_t i, int64_t j, size_t& k, size_t& l) const;

};

#endif // SPIELFELD_H