#ifndef MENUE_H
#define MENUE_H

#include <list>
#include <string>
#include <iostream>

/**
 *
 */
class MenueEintrag
{
public:
  typedef bool (*handle_menue_eintrag_t) (const MenueEintrag&);

private:
  char                   m_kuerzel;
  std::string            m_titel;
  handle_menue_eintrag_t m_handler;
  
public:
  /** */
  inline std::string getTitel() const { return m_titel; }
  
  inline char getKuerzel() const { return m_kuerzel; }

  /** Default constructor */
  MenueEintrag(char kuerzel, const std::string& titel, handle_menue_eintrag_t handler);

  /** Default destructor */
  virtual ~MenueEintrag();

  /** Copy constructor
   *  \param other Object to copy from
   */
  MenueEintrag(const MenueEintrag& other);

  /** Assignment operator
   *  \param other Object to assign from
   *  \return A reference to this
   */
  MenueEintrag& operator=(const MenueEintrag& other);

  bool operator () () const;

protected:
private:
};

/**
 *
 */
class Menue
{
private:
  /** datentyp liste von eintraegen */
  typedef std::list<MenueEintrag> eintraege_t;

  /** liste der menueeintraege */
  eintraege_t m_eintraege;
  
  /** */
  std::string m_titel;

  /**  */
  std::string m_aufforderung;

  /**  */
  std::string m_fehlermeldung;

  /** */
  std::istream* m_in;

  /** */
  std::ostream* m_out;

public:
  /** Default constructor */
  Menue(const std::string& titel, const std::string& aufforderung, const std::string& fehlermeldung, std::istream& in = std::cin, std::ostream& out = std::cout);

  /** Default destructor */
  virtual ~Menue();

  /** Copy constructor
   *  \param other Object to copy from
   */
  Menue(const Menue& other);
  
  /** */
  std::string getTitel() const { return m_titel; }
  
  /** */
  std::string getAufforderung() const { return m_aufforderung; }  

  /** */
  std::string getFehlermeldung() const { return m_fehlermeldung; }  

  /** Assignment operator
   *  \param other Object to assign from
   *  \return A reference to this
   */
  Menue& operator=(const Menue& other);

  /**
   *
   */
  void addEintrag(const MenueEintrag& eintrag);

  /** Menue anzeigen */
  bool anzeigen() const;

protected:
private:
};

#endif // MENUE_H