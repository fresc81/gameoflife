#ifndef DIRECTORY_H_INCLUDED
#define DIRECTORY_H_INCLUDED

#include <dirent.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <string>
#include <list>


namespace fs
{

  /**
   * helper class for browsing a POSIX filesystem
   */
  struct directory
  {
    typedef dirent *ent_t;                  // directory entry type
    typedef std::list< std::string > seg_t; // list of path segments type

    /**
     * get current working directory
     */
    inline static std::string cwd()
    {
      char buffer[PATH_MAX];
      ::memset(buffer, 0, PATH_MAX);
      return std::string(::getcwd(buffer, PATH_MAX));
    }

    /**
     * change current working directory
     */
    inline static bool cd(const std::string& path)
    {
      return ::chdir(path.c_str()) == 0;
    }

    /**
     * resolve a relative path and return the absolute filesystem path
     */
    inline static std::string resolve(const std::string& path)
    {
      return ::realpath(path.c_str(), NULL);
    }

    /**
     * return a list of segments that make up the path, for absolute paths the first segment is always empty
     */
    inline static seg_t explode(const std::string& path)
    {
      seg_t segments;
      std::string segment;
      std::string::const_iterator it = path.begin();

      for (;it != path.end(); it++)
      {
        if (*it == '/')
        {
          if ((segment.length() > 0) || (segments.size() == 0))
            segments.push_back(segment);
          segment.clear();
        } else
        {
          segment.push_back(*it);
        }
      }

      if (!segment.empty())
        segments.push_back(segment);

      return segments;
    }

    /**
     * return the path made out of the given segments, for absolute paths the first segment needs to be empty
     */
    inline static std::string implode(const seg_t& segments)
    {
      std::string path;
      seg_t::const_iterator it = segments.begin();

      for (; it != segments.end(); it++)
      {
        if (it == segments.begin())
        {
          path = it->empty() ? "/" : *it;
        } else
        {
          if (!it->empty())
            path += (path == "/") ? *it : ("/" + *it);
        }
      }

      return path;
    }

  private:
    /**
     * recursive method for normalizing the path given as segments
     */
    inline static std::string _normalize(seg_t& segments)
    {
      seg_t::iterator it = segments.begin();
      seg_t::iterator prev = segments.end();

      for (; it != segments.end(); it++)
      {
        if ((it != segments.begin()) && it->empty()) // eliminate '' if not first segment
        {
          segments.erase(it);

          // recursive
          return _normalize(segments);
        } else if (*it == ".")                       // eliminate '.'
        {
          segments.erase(it);

          // recursive
          return _normalize(segments);
        } else if (*it == "..")                      // eliminate '..'
        {

          if ((prev != segments.end()) && (*prev != ".") && (*prev != ".."))
          {
            if (!(prev==segments.begin() && prev->empty()))
              segments.erase(prev);
            segments.erase(it);

            // recursive
            return _normalize(segments);
          }

        }

        prev = it;
      }
      return implode(segments);
    }

    inline static bool is_absolute(const seg_t& segments)
    {
      return segments.size() > 0 && segments.front().empty();
    }

  public:
    /**
     * normalize the path and resolve as much of the virtual directories (. and ..) as possible
     */
    inline static std::string normalize(const std::string& path)
    {
      seg_t segments = explode(path);
      return _normalize(segments);
    }

    /**
     * combines a basepath with another path
     */
    inline static std::string combine(const std::string& base, const std::string& path)
    {
      seg_t seg_path = explode(path);
      if (is_absolute(seg_path))
        return _normalize(seg_path);

      seg_t seg_base = explode(base);
      seg_t segments;
      segments.insert(segments.end(), seg_base.begin(), seg_base.end());
      segments.insert(segments.end(), seg_path.begin(), seg_path.end());
      return _normalize(segments);
    }

  private:
    typedef DIR *dir_t; // directory pointer type

    std::string m_path; // directory path
    dir_t m_dir;        // directory pointer
    ent_t m_ent;        // directory entry

  public:
    /**
     * construct a directory object which refers to the current working directory
     */
    inline directory():
      m_path(normalize(cwd())),
      m_dir(NULL),
      m_ent(NULL)
    {
    }

    /**
     * construct a directory object which refers to the given path
     */
    inline directory(const std::string& path):
      m_path(normalize(path)),
      m_dir(NULL),
      m_ent(NULL)
    {
    }

    /**
     * construct a directory object which refers to the given path relative to the base directory
     */
    inline directory(const directory& base, const std::string& path):
      m_path(combine(base.path(), path)),
      m_dir(NULL),
      m_ent(NULL)
    {
    }

    /**
     * copy constructor
     */
    inline directory(const directory& other):
      m_path(normalize(other.path())),
      m_dir(NULL),
      m_ent(NULL)
    {
    }

    /**
     * destructor
     */
    inline ~directory()
    {
      if (m_dir != NULL)
        ::closedir(m_dir);

      m_dir = NULL;
      m_ent = NULL;
    }

    /**
     * assign the path of the other directory object
     */
    inline directory& operator = (const directory& other)
    {
      if (this == &other)
        return *this;

      if (m_dir != NULL)
        ::closedir(m_dir);

      m_path = normalize(other.path());
      m_dir = NULL;
      m_ent = NULL;

      return *this;
    }

    /**
     * opens the directory path if not already done and returns true if has been successfully opened
     */
    inline bool valid()
    {
      if (m_dir == NULL)
        m_dir = ::opendir(m_path.c_str());

      return m_dir != NULL;
    }

    /**
     * returns the path of this directory
     */
    inline std::string path() const
    {
      return m_path;
    }

    /**
     * returns the entry previously read
     */
    inline ent_t entry() const
    {
      return m_ent;
    }

    /**
     * returns the relative path of this directory or the entry previously read
     */
    inline std::string relative() const
    {
      if (entry() == NULL)
        return path();

      return combine(path(), entry()->d_name);
    }

    /**
     * returns the absolute path of this directory or the entry previously read
     */
    inline std::string absolute() const
    {
      return resolve(relative());
    }

    /**
     * returns the path of this directory's parent directory
     */
    inline std::string parent() const
    {
      return combine(path(), "..");
    }

    /**
     * returns a new directory object made of the relative path of this directory or the entry previously read
     */
    inline directory relative_directory() const
    {
      return directory(relative());
    }

    /**
     * returns a new directory object made of the absolute path of this directory or the entry previously read
     */
    inline directory absolute_directory() const
    {
      return directory(absolute());
    }

    /**
     * returns a new directory object made of the path of this directory's parent directory
     */
    inline directory parent_directory() const
    {
      return directory(parent());
    }

    /**
     * make this directory the current working directory
     */
    inline bool cd()
    {
      return cd(path());
    }

    /**
     * rewind the directory reader
     */
    inline void rewind()
    {
      if (valid())
        ::rewinddir(m_dir);

      m_ent = NULL;
    }

    /**
     * read next directory entry, returns NULL after the last entry has been read
     */
    inline ent_t read()
    {
      return m_ent = valid() ? ::readdir(m_dir) : NULL;
    }

#ifdef HAVE_SEEKDIR_TELLDIR

    inline long int tell()
    {
      return valid() ? ::telldir(m_dir) : NULL;
    }

    inline void seek(long int pos)
    {
      if (valid())
        ::seekdir(m_dir, pos);
    }

#endif

  };
}

#endif
