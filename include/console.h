#ifndef CONSOLE_H_INLCUDED
#define CONSOLE_H_INLCUDED

#include <iostream>
#include <string>
#include <functional>
#include <limits>

namespace console {

  void cursorxy( int col = 1, int line = 1 );
  void clear( bool gohome = true );
  void size( unsigned short& width, unsigned short& height );

  inline std::string escape ( const std::string& sequence )
  {
    return std::string("\x1b[")+sequence;
  }

  inline void cursorxy( int col, int line )
  {
    std::cout << escape("") << line << ";" << col << "H" << std::flush;
  }

  inline void clear( bool gohome )
  {
    std::cout << escape("2J") << std::flush;
  }

  inline bool scan_line ( const std::string& prompt, std::string& line )
  {
    using namespace std;
    cout << prompt;
    cin.clear();
    return std::getline(cin, line).good();
  }

  template <typename ValueT>
  inline bool scan ( const std::string& prompt, const std::string& errmsg, ValueT& value )
  {
    using namespace std;

    for (;;)
    {
      if (cin.eof())
        return false;
      
      cout << prompt;
      cin >> skipws >> value;
		
      if (!cin.fail())
        break;
      
      if (cin.bad())
        return false;
		
		cin.clear();
		{
			string line;
			getline(cin, line);
		}
      cout << errmsg << endl;
    }
    return true;
  }

  template <typename ValueT>
  inline bool scan_limit (
    const std::string& prompt, const std::string& errmsg, ValueT& value,
    const ValueT& min = std::numeric_limits<ValueT>::min(),
    const ValueT& max = std::numeric_limits<ValueT>::max()
  )
  {
    using namespace std;

    for (;;)
    {

      if (!scan(prompt, errmsg, value))
        return false;

      if ((value >= min) && (value < max))
        break;

      cout << errmsg << endl;
    }
    return true;
  }

}

#endif // CONSOLE_H_INLCUDED
