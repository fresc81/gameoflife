#ifndef DEBUG_H_INCLUDED
#define DEBUG_H_INCLUDED

#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>

#ifdef DEBUG

#define DEBUG_INIT() std::string debug::m_indent;

#define DEBUG_UNIT(logfile) static debug __dbg_unit__(__FILE__, (logfile));

#define DEBUG_FUNCTION() debug::function __dbg_function__ = __dbg_unit__.call(__PRETTY_FUNCTION__);

struct debug
{
  struct function
  {
    debug&      m_debug;
    std::string m_name;

    inline function(debug& dbg, const std::string& name):
      m_debug(dbg),
      m_name(name)
    {
      m_debug.log(m_debug.m_indent+">> "+m_name);
      m_debug.m_indent += ' ';
    }

    inline ~function()
    {
      m_debug.m_indent = m_debug.m_indent.substr(0, m_debug.m_indent.length() - 1);
      m_debug.log(m_debug.m_indent+"<< "+m_name);
    }

  };

  std::string   m_subject;
  std::ostream *m_out;
  bool          m_managed;
  std::string   m_prefix;
  std::string   m_suffix;

  static std::string m_indent;

  inline debug(
    const std::string& subject,
    std::ostream& out = std::clog,
    const std::string& prefix = "",
    const std::string& suffix = ""
  ):
    m_subject(subject),
    m_out(&out),
    m_managed(false),
    m_prefix(prefix),
    m_suffix(suffix)
  {
  }

  inline debug(
    const std::string& subject,
    const std::string& filename,
    const std::string& prefix = "",
    const std::string& suffix = ""
  ):
    m_subject(subject),
    m_out(new std::ofstream(filename.c_str())),
    m_managed(true),
    m_prefix(prefix),
    m_suffix(suffix)
  {
  }

  inline ~debug()
  {
    if (m_managed)
      delete m_out;
  }

  inline debug& log(const std::string& value)
  {
    if (m_out && &m_indent)
	(*m_out) << std::setw(64) << m_subject << '|' << m_prefix << value << m_suffix << std::endl;
    return *this;
  }

  inline function call (const std::string& name)
  {
    return function(*this, name);
  }

};

#else // DEBUG

#define DEBUG_INIT()

#define DEBUG_UNIT(logfile)

#define DEBUG_FUNCTION()

#endif

#endif // DEBUG_H_INCLUDED
