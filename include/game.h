#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#include "spielfeld.h"
#include "regeln.h"

#include <deque>
#include <vector>

enum SimulationsErgebnis
{
  SIMULATION_OKAY,
  SIMULATION_BEREITS_GESTARTET,
  SIMULATION_KONSOLE_ZU_KLEIN,
  
  SIMULATION_KEINE_AENDERUNG_MEHR
};

class GameOfLife
{
  typedef std::deque<Spielfeld> spielfeld_queue_t;
  
public:
  typedef std::vector<std::string> zelle_aussehen_t;

private:
  Spielfeld           m_spielfeld;
  Regeln              m_regeln;
  zelle_aussehen_t    m_aussehen;
  spielfeld_queue_t   m_historie;
  
public:
  GameOfLife();
  GameOfLife(const Spielfeld& spielfeld, const Regeln& regeln, const zelle_aussehen_t& aussehen);
  GameOfLife(const GameOfLife& other);
  virtual ~GameOfLife();

  GameOfLife& operator = (const GameOfLife& other);
  
  inline const Spielfeld& getSpielfeld() const { return m_spielfeld; }
  
  inline Spielfeld& getSpielfeld() { return m_spielfeld; }
  
  inline void setSpielfeld(const Spielfeld& spielfeld) { m_spielfeld = spielfeld; }
  
  inline const Regeln& getRegeln() const { return m_regeln; }
  
  inline Regeln& getRegeln() { return m_regeln; }
  
  inline void setRegeln(const Regeln& regeln) { m_regeln = regeln; }
  
  inline const zelle_aussehen_t& getAussehen() const { return m_aussehen; }
  
  inline zelle_aussehen_t& getAussehen() { return m_aussehen; }
  
  inline void setAussehen(const zelle_aussehen_t& aussehen) { m_aussehen = aussehen; }
  
  void nextSpielzug();
  
  bool hasKeineAenderungMehr();

  static SimulationsErgebnis simulationStarten(GameOfLife& game);

private:
  static bool checkConsoleSize(const Spielfeld& spielfeld);

};

/** Spielfeld ausgeben */
std::ostream& operator << (std::ostream& out, const GameOfLife& game);

/*
std::istream& operator >> (std::istream& in, GameOfLife& game);
*/

#endif //  GAME_H_INCLUDED