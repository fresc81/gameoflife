#ifndef MATRIX_H_INCLUDED
#define MATRIX_H_INCLUDED

#include <vector>
#include <exception>

// eine ausnahme, abgeleitet von std::exception
struct index_out_of_bounds_exception : public std::exception
{
};

// matrix datentyp
template <typename ValueT>
struct matrix
{
  // typendefinitionen / abkürzungen (_type suffix)
  typedef ValueT                          value_type;

private:
  typedef std::vector< value_type >       line_vector_type;
  typedef std::vector< line_vector_type > matrix_type;

public:
  // referenz auf value_type (einmal konstant und einmal veränderlich)
  typedef typename line_vector_type::reference       reference;
  typedef typename line_vector_type::const_reference const_reference;

private:
  // klassenvariablen (m_ präfix)
  size_t      m_n;
  size_t      m_m;
  matrix_type m_data;

public:
  // ctor
  matrix(size_t n, size_t m, const value_type& value = value_type()):
    m_n(n),
    m_m(m),
    m_data(m_n, line_vector_type(m_m, value))
  {
  }

  // copy ctor: matrix<...> matrixA(matrixB);
  matrix(const matrix<value_type>& other):
    m_n(other.m_n),
    m_m(other.m_m),
    m_data(other.m_data)
  {
  }

  // dtor
  virtual ~matrix()
  {
  }

  // matrixA = matrixB
  virtual matrix<value_type>& operator = (const matrix<value_type>& other)
  {
    if (this == &other)
      return *this;

    m_n = other.m_n;
    m_m = other.m_m;
    m_data = other.m_data;
    return *this;
  }

  // getter für n
  inline size_t getN() const { return m_n; }

  // getter für m
  inline size_t getM() const { return m_m; }
  
  void clear (const value_type& value = value_type())
  {
    for (size_t i = 0; i < m_n; i++)
      for (size_t j = 0; j < m_m; j++)
        m_data[i][j] = value;
  }

  void resize (size_t n, size_t m, const value_type& value = value_type())
  {
    matrix_type altData(m_data);
    const size_t altN = m_n, altM = m_m;

    m_n = n;
    m_m = m;
    m_data = matrix_type(m_n, line_vector_type(m_m, value));

    for (size_t i = 0; i < m_n; i++)
    {
      if (i<altN)
        for (size_t j = 0; j < m_m; j++)
          if (j<altM)
            m_data[i][j] = altData[i][j];
    }
  }

  // referenz auf matrixelement zurückgeben
  // -> referenz ist wie ein zeiger (nur als klasse ohne *)
  reference operator () (size_t i, size_t j) throw (index_out_of_bounds_exception)
  {
    if ((i>=m_n) || (j>=m_m))
      throw index_out_of_bounds_exception();

    return m_data[i][j];
  }

  // konstante referenz auf matrixelement zurückgeben
  // -> wenn element irgendwo anders (im normalen kontext) geändert wird,
  //    zeigt die konstante referenz auch auf den neuen wert, man kann über die
  //    konstante referenz nicht schreiben - nur lesen
  const_reference operator () (size_t i, size_t j) const throw (index_out_of_bounds_exception)
  {
    if ((i>=m_n) || (j>=m_m))
      throw index_out_of_bounds_exception();

    return m_data[i][j];
  }

  // vergleiche zwei matritzen
  bool operator == (const matrix<value_type>& other) const
  {
    if ((m_n != other.m_n) || (m_m != other.m_m))
      return false;

    for (size_t i = 0; i < m_n; i++)
      for (size_t j = 0; j < m_m; j++)
        if (m_data[i][j] != other.m_data[i][j])
          return false;

    return true;
  }

  bool operator != (const matrix<value_type>& other) const
  {
    if ((m_n != other.m_n) || (m_m != other.m_m))
      return true;

    for (size_t i = 0; i < m_n; i++)
      for (size_t j = 0; j < m_m; j++)
        if (m_data[i][j] != other.m_data[i][j])
          return true;

    return false;
  }

  // ! operator als operator zum transponieren missbrauchen ];{)
  matrix<value_type> operator ! () const
  {
    matrix<value_type> result(m_m, m_n);

    for (size_t i = 0; i < m_n; i++)
      for (size_t j = 0; j < m_m; j++)
        result.m_data[j][i] = m_data[i][j];

    return result;
  }

};

#endif // MATRIX_H_INCLUDED