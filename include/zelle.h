#ifndef ZELLE_INCLUDED_H
#define ZELLE_INCLUDED_H

typedef size_t zelle_zustand;

enum zelle_nachbarn
{
  NACHBARN_NEUMANN = 4,
  NACHBARN_MOORE   = 8
};

enum
{
  ZELLE_TOT,
  ZELLE_LEBT,
  
  ZELLE_TYPA = ZELLE_LEBT,
  ZELLE_TYPB,
  ZELLE_TYPC,
  ZELLE_TYPD
  
};

#endif // ZELLE_INCLUDED_H
