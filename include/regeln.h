#ifndef REGELN_H
#define REGELN_H

#include "spielfeld.h"

#include <list>
#include <string>

class Regeln : public matrix<zelle_zustand>
{
private:
  double         m_zufaellige_geburtenrate;
  double         m_zufaellige_sterberate;

public:
  Regeln(
    double         zufaellige_geburtenrate = 0.0,            // warscheinlichkeit, dass neue zellen entstehen
    double         zufaellige_sterberate   = 0.0,            // warscheinlichkeit, dass zellen sterben
    zelle_nachbarn nachbarn                = NACHBARN_MOORE, // nachbar algorithmus (moore oder neumann)
    size_t         zelltypen               = 4               // anzahl der zelltypen (ohne tote zelle)
  );

  Regeln(const Regeln& other);

  virtual ~Regeln();
  
  inline double getZufaelligeGeburtenrate() const
  { return m_zufaellige_geburtenrate; }
  
  inline void setZufaelligeGeburtenrate(double zufaellige_geburtenrate)
  { m_zufaellige_geburtenrate = zufaellige_geburtenrate; }

  inline double getZufaelligeSterberate() const
  { return m_zufaellige_sterberate; }

  inline void setZufaelligeSterberate(double zufaellige_sterberate)
  { m_zufaellige_sterberate = zufaellige_sterberate; }

  inline zelle_nachbarn getNachbarn()
  { return static_cast<zelle_nachbarn>(getM()-1); }
  
  inline void setNachbarn(zelle_nachbarn nachbarn)
  { resize(getN(), static_cast<size_t>(nachbarn+1)); }
  
  inline size_t getZelltypen()
  { return getM(); }
  
  inline void setZelltypen(size_t zelltypen)
  { resize(zelltypen, getM()); }
  
/*  
  std::string toString() const;
  
  void fromString(const std::string& welt);
*/

  Spielfeld anwenden(const Spielfeld& spielfeld);

};

#endif // REGELN_H