
Game Of Life
============

Spiel des Lebens - ein Simulator für Zelluläre Automaten.

Features
--------

 * Spielfeld
    * Inhalt ausgeben
    * Größe ändern
    * Zufallsgenerator mit Warscheinlichkeit
    * Datei laden
    * Datei speichern
 * Regeln
    * 23/3      Conway (Original)
    * 1357/1357 Kopierwelt
    * Benutzerdefinierte Regel (Matrix)
    * Spontane Sterbe- und Geburtsrate mit Warscheinlichkeit
 * Simulation bis keine Änderung mehr
    * Aussehen der Zellen frei wählbar, inkl. Farbe
    * unterschiedliche Zelltypen möglich

Installation
------------

### Release Packet ###

Das Release-Packet hat die Form gameoflife-VERSION.tar.gz und kann auf dem
üblichen Weg (./configure && make && sudo make install) installiert werden.

		$ tar vxzf gameoflife-1.5.tar.gz
		  ... (archiv auspacken)
		$ cd gameoflife-1.5
		$ ./configure
		  ... (configure Skript)
		$ make
		  ... (Programm kompilieren)
		$ sudo make install
		  ... (Programm installieren)
		
### GIT Quellen ###

Um direkt aus den GIT-Quellen zu bauen und installieren müssen die Makefiles
und das configure Skript neu generiert werden (autoreconf).

		$ git clone https://bitbucket.org/fresc81/GameOfLife.git gameoflife-master
		  ... (GIT Quellen auschecken)
		$ cd gameoflife
		$ autoreconf -vfi
		  ... (erstellt configure Skript und Makefile.in Dateien)
		$ mkdir build && cd build
		  ... (das Verzeichnis "build" wird von GIT ignoriert)
		$ ../configure
		  ... (configure Skript)
		$ make
		  ... (Programm kompilieren)
		$ sudo make install
		  ... (Programm installieren)
