#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include "debug.h"
#ifdef DEBUG
extern std::ofstream s_logfile;
#endif
DEBUG_UNIT(s_logfile)

#include "regeln.h"
#include <cstdlib>
#include <cstring>

#include <set>
#include <algorithm>

/** return random number [0;1) */
static double random_double ()
{
  DEBUG_FUNCTION()

  using namespace std;
  return ((double) rand() / (RAND_MAX));
}

Regeln::Regeln(
  double zufaellige_geburtenrate,
  double zufaellige_sterberate,
  zelle_nachbarn nachbarn,
  size_t zelltypen
):
  matrix<zelle_zustand>(zelltypen, nachbarn+1),
  m_zufaellige_geburtenrate(zufaellige_geburtenrate),
  m_zufaellige_sterberate(zufaellige_sterberate)
{
  DEBUG_FUNCTION()

}

Regeln::Regeln(const Regeln& other):
  matrix<zelle_zustand>((const matrix<zelle_zustand>&)other),
  m_zufaellige_geburtenrate(other.m_zufaellige_geburtenrate),
  m_zufaellige_sterberate(other.m_zufaellige_sterberate)
{
  DEBUG_FUNCTION()

}

Regeln::~Regeln()
{
  DEBUG_FUNCTION()

}

/*
struct to_string_helper
{
private:
  std::string m_string;

public:
  to_string_helper(const std::string& s = ""):
    m_string(s)
  {
  }

  to_string_helper(const to_string_helper& other):
    m_string(other.m_string)
  {
  }

  inline void operator () (const char& c)
  {
    char buffer[] = { 0, 0 };
    buffer[0] = (char)( (int)'0' + (int)c );
    append(buffer);
  }

  inline void append(const std::string& s)
  {
    m_string.append(s);
  }

  inline std::string toString()
  {
    return m_string;
  }

};

std::string Regeln::toString() const
{
  std::set<char> leben, geburt;

  {
    const char chars[] = { 0,1,2,3,4,5,6,7,8 };
    leben.insert(chars, chars+9);
  }

  for (size_t c = 0; c < 9; c++)
  {
    regel_list_t::const_iterator it = m_regeln.begin();
    regel_list_t::const_iterator end = m_regeln.end();

    for ( ;it != end; it++)
    {

      switch (it->getZustand())
      {
      case ZELLE_TOT:
        if (it->checkRelation(c))
          leben.erase((char)c);
        break;

      case ZELLE_LEBT:
        if (it->checkRelation(c))
          geburt.insert((char)c);
        break;

      }

    }

  }

  to_string_helper helper;
  helper = std::for_each(leben.begin(), leben.end(), helper);
  helper.append("/");
  helper = std::for_each(geburt.begin(), geburt.end(), helper);

  return helper.toString();
}

struct from_string_helper
{
private:
  Regeln *m_regeln;
  zelle_zustand m_zustand;

public:
  from_string_helper(Regeln *regeln, zelle_zustand zustand):
    m_regeln(regeln),
    m_zustand(zustand)
  {
  }

  from_string_helper(const from_string_helper& other):
    m_regeln(other.m_regeln),
    m_zustand(other.m_zustand)
  {
  }

  inline void operator () (const char& c)
  {
    m_regeln->addRegel(Regel(GLEICH, c-'0', m_zustand));
  }

};

void Regeln::fromString(const std::string& welt)
{
  // split at /
  // read into set<char> leben, geburt
  std::string::size_type found = welt.find_first_of("/");
  if (found != std::string::npos)
  {
    const std::string first = welt.substr(0, found);
    const std::string second = welt.substr(found+1);

    std::set<char> leben, geburt, sterben;
    leben.insert(first.begin(), first.end());
    geburt.insert(second.begin(), second.end());

    {
      const std::string chars = "012345678";
      sterben.insert(chars.begin(), chars.end());
      for (std::set<char>::iterator it = leben.begin(); it != leben.end(); it++)
        sterben.erase(*it);
      for (std::set<char>::iterator it = geburt.begin(); it != geburt.end(); it++)
        sterben.erase(*it);
    }

    clear();
    std::for_each(sterben.begin(), sterben.end(), from_string_helper(this, ZELLE_TOT));
    std::for_each(geburt.begin(), geburt.end(), from_string_helper(this, ZELLE_LEBT));

  }
}
*/

Spielfeld Regeln::anwenden(const Spielfeld& spielfeld)
{
  DEBUG_FUNCTION()

  const int64_t zeilen = spielfeld.getN();
  const int64_t spalten = spielfeld.getM();

  Spielfeld result(spielfeld); // spielfeld klonen
  double rng;

  for (int64_t i = 0; i < zeilen; i++)
  {
    for (int64_t j = 0; j < spalten; j++)
    {
      switch (getNachbarn()) // moore oder von neumann nachbarn
      {

      case NACHBARN_NEUMANN:
        result(i, j) = operator () (spielfeld(i, j), spielfeld.nachbarn_neumann(i, j));
        break;

      case NACHBARN_MOORE:
        result(i, j) = operator () (spielfeld(i, j), spielfeld.nachbarn_moore(i, j));
        break;

      }

      if (spielfeld(i,j) == result(i,j)) // wenn zelle unverändert
      {
        // TODO: zelltypen berücksichtigen
        rng = random_double();
        if (spielfeld(i, j) != ZELLE_TOT)
        {
          if (rng <= m_zufaellige_sterberate)
            result(i, j) = ZELLE_TOT;
        } else {
          if (rng <= m_zufaellige_geburtenrate)
            result(i, j) = ZELLE_LEBT;
        }
      }

    }
  }

  return result;
}