#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include "debug.h"
#ifdef DEBUG
extern std::ofstream s_logfile;
#endif
DEBUG_UNIT(s_logfile)

#include "spielfeld.h"

#include <set>

using namespace std;

Spielfeld::Spielfeld(size_t n, size_t m):
  matrix<zelle_zustand>(n, m)
{
  DEBUG_FUNCTION()

  //ctor
}

Spielfeld::~Spielfeld()
{
  DEBUG_FUNCTION()

  //dtor
}

Spielfeld::Spielfeld(const Spielfeld& other):
  matrix<zelle_zustand>(other)
{
  DEBUG_FUNCTION()

  //copy ctor
}

Spielfeld& Spielfeld::operator = (const Spielfeld& other)
{
  DEBUG_FUNCTION()

  // handle self assignment
  if (this == &other)
    return *this;

  //assignment operator
  dynamic_cast< matrix<zelle_zustand>& >(*this) = dynamic_cast<const matrix<zelle_zustand>& >(other);

  return *this;
}

Spielfeld::reference Spielfeld::operator () (int64_t i, int64_t j)
{
  // DEBUG_FUNCTION()

  size_t k, l;
  index(i, j, k, l);
  return dynamic_cast< matrix<zelle_zustand>& >(*this)(k,l);
}

Spielfeld::const_reference Spielfeld::operator () (int64_t i, int64_t j) const
{
  // DEBUG_FUNCTION()

  size_t k, l;
  index(i, j, k, l);
  return dynamic_cast< const matrix<zelle_zustand>& >(*this)(k,l);
}

size_t Spielfeld::anzahl_zelltypen () const
{
  DEBUG_FUNCTION()
  set<zelle_zustand> zustaende;
  
  for (size_t i = 0; i < getN(); i++)
    for (size_t j = 0; j < getM(); j++)
      zustaende.insert(dynamic_cast< const matrix<zelle_zustand>& >(*this)(i, j));
  
  return zustaende.size();
}

size_t Spielfeld::nachbarn_moore (int64_t i, int64_t j) const
{
  // DEBUG_FUNCTION()

  size_t k, l;
  size_t result = 0;

  index(i-1, j-1, k, l);
  if ((*this)(k, l))
    ++result;
  index(i-1, j, k, l);
  if ((*this)(k, l))
    ++result;
  index(i-1, j+1, k, l);
  if ((*this)(k, l))
    ++result;

  index(i, j-1, k, l);
  if ((*this)(k, l))
    ++result;
  index(i, j+1, k, l);
  if ((*this)(k, l))
    ++result;

  index(i+1, j-1, k, l);
  if ((*this)(k, l))
    ++result;
  index(i+1, j, k, l);
  if ((*this)(k, l))
    ++result;
  index(i+1, j+1, k, l);
  if ((*this)(k, l))
    ++result;

  return result;
}

size_t Spielfeld::nachbarn_neumann (int64_t i, int64_t j) const
{
  // DEBUG_FUNCTION()

  size_t k, l;
  size_t result = 0;

  index(i-1, j, k, l);
  if (this->operator () (k, l))
    ++result;

  index(i, j-1, k, l);
  if (this->operator () (k, l))
    ++result;

  index(i, j+1, k, l);
  if (this->operator () (k, l))
    ++result;

  index(i+1, j, k, l);
  if (this->operator () (k, l))
    ++result;

  return result;
}

void Spielfeld::index(int64_t i, int64_t j, size_t& k, size_t& l) const
{
  // DEBUG_FUNCTION()

  const int64_t n = getN(), m = getM();

  while (i < 0)
    i += n;

  while (i >= n)
    i -= n;

  while (j < 0)
    j += m;

  while (j >= m)
    j -= m;

  k = i;
  l = j;
}