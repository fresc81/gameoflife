#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include "debug.h"
DEBUG_INIT()
#ifdef DEBUG
std::ofstream s_logfile("logfile.txt");
#endif
DEBUG_UNIT(s_logfile)


#include "game.h"
#include "menue.h"
#include "console.h"
#include "directory.h"

#include <cstdlib>

using namespace std;
using namespace fs;
using namespace console;

static GameOfLife s_game;
static size_t s_width = 64;
static size_t s_height = 24;

static void regeln_setup_323_545_767()
{
  DEBUG_FUNCTION()

  // regeln erstellen
  Regeln& regeln = s_game.getRegeln();

  regeln.clear();

  regeln(ZELLE_TOT,  3) = ZELLE_TYPA;
  regeln(ZELLE_TYPA, 2) = ZELLE_TYPA;
  regeln(ZELLE_TYPA, 3) = ZELLE_TYPA;

  regeln(ZELLE_TOT,  5) = ZELLE_TYPB;
  regeln(ZELLE_TYPB, 4) = ZELLE_TYPB;
  regeln(ZELLE_TYPB, 5) = ZELLE_TYPB;

  regeln(ZELLE_TOT,  7) = ZELLE_TYPC;
  regeln(ZELLE_TYPC, 6) = ZELLE_TYPC;
  regeln(ZELLE_TYPC, 7) = ZELLE_TYPC;
}

static void regeln_setup_323()
{
  DEBUG_FUNCTION()

  Regeln& regeln = s_game.getRegeln();
  
  regeln.clear();
  
  regeln(ZELLE_TOT,  3) = ZELLE_LEBT;
  regeln(ZELLE_LEBT, 2) = ZELLE_LEBT;
  regeln(ZELLE_LEBT, 3) = ZELLE_LEBT;
}

static void regeln_setup_1357()
{
  DEBUG_FUNCTION()

  Regeln& regeln = s_game.getRegeln();
  
  regeln.clear();
  
  regeln(ZELLE_TOT, 1) = ZELLE_LEBT;
  regeln(ZELLE_TOT, 3) = ZELLE_LEBT;
  regeln(ZELLE_TOT, 5) = ZELLE_LEBT;
  regeln(ZELLE_TOT, 7) = ZELLE_LEBT;
}

static void init_regeln ()
{
  DEBUG_FUNCTION()

  regeln_setup_323();
}

static void init_spielfeld ()
{
  DEBUG_FUNCTION()

  // spielfeld erstellen
  Spielfeld spielfeld(s_height, s_width);
  s_game.setSpielfeld(spielfeld);
}

// regeln und spielfeld erstellen, variabeln initialisieren
static void init_game ()
{
  DEBUG_FUNCTION()

  srand((unsigned int) time(NULL));

  GameOfLife::zelle_aussehen_t aussehen(5);
  aussehen[ZELLE_TOT ] = escape("37;40m")+"."+escape("m"); // leer -> schwarz
  aussehen[ZELLE_TYPA] = escape("30;41m")+"."+escape("m"); // typa -> rot
  aussehen[ZELLE_TYPB] = escape("30;42m")+"."+escape("m"); // typb -> grün
  aussehen[ZELLE_TYPC] = escape("30;44m")+"."+escape("m"); // typc -> blau
  aussehen[ZELLE_TYPD] = escape("30;43m")+"."+escape("m"); // typd -> gelb
  s_game.setAussehen(aussehen);

  init_regeln();
  init_spielfeld();
}

// aufräumen
static void exit_game ()
{
  DEBUG_FUNCTION()


}

// menü zurück/beenden
static bool menu_abbrechen (const MenueEintrag& eintrag)
{
  DEBUG_FUNCTION()

  return false;
}

// hauptmenü > spielfeld  ausgeben
static bool menu_simulation_ausgabe (const MenueEintrag& eintrag)
{
  DEBUG_FUNCTION()

  cout << s_game << endl;
  return true;
}

// hauptmenü > simulationsschritt
static bool menu_simulation_schritt (const MenueEintrag& eintrag)
{
  DEBUG_FUNCTION()

  s_game.nextSpielzug();
  cout << s_game << endl;
  return true;
}

// hauptmenü > simulation starten
static bool menu_simulation_starten (const MenueEintrag& eintrag)
{
  DEBUG_FUNCTION()

  switch (GameOfLife::simulationStarten(s_game))
  {
  case SIMULATION_OKAY:
    cout << "Simulation unterbrochen" << endl;
    break;
  case SIMULATION_BEREITS_GESTARTET:
    cout << "Simulation bereits gestartet" << endl;
    break;
  case SIMULATION_KONSOLE_ZU_KLEIN:
    cout << "Konsole zu klein um das Feld darzustellen" << endl;
    break;
  case SIMULATION_KEINE_AENDERUNG_MEHR:
    cout << "Spielfeld ändert sich nicht mehr" << endl;
    break;
  }
  return true;
}

static bool menu_spielfeld_groesse (const MenueEintrag& eintrag)
{
  DEBUG_FUNCTION()

  unsigned int width, height;

  console::scan_limit<unsigned int>(
    "Spielfeldbreite [>1]? ",
    escape("31m")+"Bitte geben Sie einen gültigen Wert ein."+escape("m"),
    width, 1
   );
  console::scan_limit<unsigned int>(
    "Spielfeldhöhe [>1]? ",
    escape("31m")+"Bitte geben Sie einen gültigen Wert ein."+escape("m"),
    height, 1
  );

  s_width = width;
  s_height = height;
  Spielfeld spielfeld = s_game.getSpielfeld();
  spielfeld.resize(height, width);
  s_game.setSpielfeld(spielfeld);

  return true;
}

static bool menu_spielfeld_neu (const MenueEintrag& eintrag)
{
  DEBUG_FUNCTION()

  init_spielfeld();
  return true;
}

static bool menu_spielfeld_zufall (const MenueEintrag& eintrag)
{
  DEBUG_FUNCTION()

  double ratioLebend = 0.0;
  console::scan_limit<double>(
    "Anteil lebendige [0..1]? ",
    escape("31m")+"Bitte geben Sie einen gültigen Wert ein."+escape("m"),
    ratioLebend, 0.0, 1.0
  );

  Spielfeld& spielfeld = s_game.getSpielfeld();

  const size_t width = spielfeld.getM(), height = spielfeld.getN();
  double rng;
  for (size_t i = 0; i < height; i++)
  {
    for (size_t j = 0; j < width; j++)
    {
      rng = (double) rand() / (RAND_MAX);
      spielfeld(i, j) = (rng <= ratioLebend) ? ZELLE_LEBT : ZELLE_TOT;
    }
  }

  //s_game.setSpielfeld(spielfeld);
  cout << s_game << endl;
  return true;
}

static string dateinamen_eingeben ()
{
  DEBUG_FUNCTION()

  directory dir;
  string input;
  for (;;)
  {

    cout << "Inhalt von [" << escape("33m") << dir.path() << escape("m") << "]" << endl;
    while (dir.read())
    {
      cout << (dir.relative_directory().valid() ?
              escape("32m")+"Verzeichnis "+escape("m") :
              escape("35m")+"Datei       "+escape("m"));
      cout << string(dir.entry()->d_name) << endl;
    }
    dir.rewind();

    input = "";
    if (!console::scan_line("Dateiname oder Ordner? ",input))
      return string();

    dir = (input.length() > 0 && input[0] == '/')
        ? directory(input).absolute_directory()
        : directory(dir, input).absolute_directory();

    if (!dir.valid())
      return dir.path();

    dir.cd();
  }
}

static bool menu_spielfeld_laden (const MenueEintrag& eintrag)
{
  DEBUG_FUNCTION()

  ifstream datei(dateinamen_eingeben().c_str());
  if (datei.bad())
  {
    cout << escape("31m") << "Fehler beim lesen der Datei." << escape("m") << endl;
    return true;
  }

  unsigned int width, height;

  datei >> skipws >> width;
  datei >> skipws >> height;

  s_height = height;
  s_width  = width;

  Spielfeld spielfeld(height, width);

  char zelle;
  for (size_t i = 0; i < height; i++)
  {
    for (size_t j = 0; j < width; j++)
    {
      if (datei.bad())
      {
        cout << escape("31m") << "Fehler beim lesen der Datei." << escape("m") << endl;
        return true;
      }
      datei >> skipws >> zelle;
      if (zelle < '0' || zelle > '9')
      {
        cout << escape("31m")
             << "ungültiges zeichen '" << zelle
             << "' in zeile " << i + 3
             << " spalte " << j + 1
             << escape("m") << endl;
        return true;
      }

      spielfeld(i, j) = (zelle_zustand) (zelle - '0');

    }
  }

  s_game.setSpielfeld(spielfeld);
  cout << s_game << endl;
  return true;
}

static bool menu_spielfeld_speichern (const MenueEintrag& eintrag)
{
  DEBUG_FUNCTION()

  ofstream datei(dateinamen_eingeben().c_str());
  if (datei.bad())
  {
    cout << escape("31m") << "Fehler beim schreiben der Datei." << escape("m") << endl;
    return true;
  }

  Spielfeld& spielfeld = s_game.getSpielfeld();
  const size_t width = spielfeld.getM(), height = spielfeld.getN();

  datei << width << endl;
  datei << height << endl;

  for (size_t i = 0; i < height; i++)
  {
    for (size_t j = 0; j < width; j++)
    {
      if (datei.bad())
      {
        cout << escape("31m") << "Fehler beim schreiben der Datei." << escape("m") << endl;
        return true;
      }
      datei << (char)('0' + spielfeld(i, j));
    }
    datei << endl;
  }

  return true;
}

/*
static bool menu_spielfeld_editor (const MenueEintrag& eintrag)
{
  return true;
}
*/

// hauptmenü > spielfeld bearbeiten
static bool menu_spielfeld (const MenueEintrag& eintrag)
{
  DEBUG_FUNCTION()

  // menü einstellungen erstellen
  Menue m1(
    escape("33m")+"=== Spielfeld bearbeiten ==="+escape("m"),
    "Ihre Auswahl?",
    escape("31m")+"Falsche Eingabe!"+escape("m")
  );
  m1.addEintrag(MenueEintrag('1', "Spielfelgröße einstellen",  menu_spielfeld_groesse));
  m1.addEintrag(MenueEintrag('2', "Spielfeld neu",             menu_spielfeld_neu));
  m1.addEintrag(MenueEintrag('3', "Spielfeld zufällig",        menu_spielfeld_zufall));
  m1.addEintrag(MenueEintrag('4', "Spielfeld laden",           menu_spielfeld_laden));
  m1.addEintrag(MenueEintrag('5', "Spielfeld speichern",       menu_spielfeld_speichern));
//  m1.addEintrag(MenueEintrag('6', "Spielfeldeditor",           menu_spielfeld_editor));
  m1.addEintrag(MenueEintrag('0', "Zurück", menu_abbrechen));

  // anzeigen, bis zurück gewählt wurde
  while (m1.anzeigen());

  return true;
}

static bool menu_regeln_323_545_767 (const MenueEintrag& eintrag)
{
  DEBUG_FUNCTION()

  regeln_setup_323_545_767();
  
  // cout << "Regel: " << regeln.toString() << endl;
  return true;
}

static bool menu_regeln_323 (const MenueEintrag& eintrag)
{
  DEBUG_FUNCTION()

  regeln_setup_323();
  
  // cout << "Regel: " << regeln.toString() << endl;
  return true;
}

static bool menu_regeln_1357 (const MenueEintrag& eintrag)
{
  DEBUG_FUNCTION()
  
  regeln_setup_1357();
  
  // cout << "Regel: " << regeln.toString() << endl;
  return true;
}

static bool menu_regeln_benutzerdefiniert (const MenueEintrag& eintrag)
{
  DEBUG_FUNCTION()

  /*
  string regel = "";
  console::scan_line("Regeln [überleben]/[geburt]? ", regel);

  Regeln regeln = s_game.getRegeln();
  regeln.fromString(regel);
  s_game.setRegeln(regeln);

  cout << "Regel: " << regeln.toString() << endl;
  */
  return true;
}

static bool menu_regeln_warscheinlichkeiten (const MenueEintrag& eintrag)
{
  DEBUG_FUNCTION()

  double geburtenrate, sterberate;

  console::scan_limit<double>(
    "Geburtenrate [0..1]? ",
    escape("31m")+"Bitte geben Sie einen gültigen Wert ein."+escape("m"),
    geburtenrate, 0.0, 1.0
  );
  console::scan_limit<double>(
    "Sterberate [0..1]? ",
    escape("31m")+"Bitte geben Sie einen gültigen Wert ein."+escape("m"),
    sterberate, 0.0, 1.0
  );

  Regeln& regeln = s_game.getRegeln();
  regeln.setZufaelligeGeburtenrate(geburtenrate);
  regeln.setZufaelligeSterberate(sterberate);

  return true;
}

// hauptmenü > einstellungen
static bool menu_regeln (const MenueEintrag& eintrag)
{
  DEBUG_FUNCTION()

  // menü einstellungen erstellen
  Menue m1(
    escape("33m")+"=== Regeln ==="+escape("m"),
    "Ihre Auswahl?",
    escape("31m")+"Falsche Eingabe!"+escape("m"));
  m1.addEintrag(MenueEintrag('1', "Regel 23/3           (Original)",   menu_regeln_323));
  m1.addEintrag(MenueEintrag('2', "Regel 23/3 45/5 67/7 (Dreifarbig)", menu_regeln_323_545_767));
  m1.addEintrag(MenueEintrag('3', "Regel 1357/1357      (Kopierwelt)", menu_regeln_1357));
  m1.addEintrag(MenueEintrag('4', "Benutzerdefinierte Regel",          menu_regeln_benutzerdefiniert));
  m1.addEintrag(MenueEintrag('5', "Warscheinlichkeit spontaner Geburten/Tötungen eingeben", menu_regeln_warscheinlichkeiten));
  m1.addEintrag(MenueEintrag('0', "Zurück", menu_abbrechen));

  // anzeigen, bis zurück gewählt wurde
  while (m1.anzeigen());

  return true;
}

// haupteinsprungspunkt der anwendung
int main()
{
  DEBUG_FUNCTION()

  // spiel initialisieren
  init_game();

  // hauptmenü erstellen
  Menue m1(
    escape("33m")+"=== Hauptmenü ==="+escape("m"),
    "Ihre Auswahl?",
    escape("31m")+"Falsche Eingabe!"+escape("m")
  );
  m1.addEintrag(MenueEintrag('1', "Spielfeld ausgeben",            menu_simulation_ausgabe));
  m1.addEintrag(MenueEintrag('2', "Simulation nächste Generation", menu_simulation_schritt));
  m1.addEintrag(MenueEintrag('3', "Simulation starten/fortsetzen", menu_simulation_starten));
  m1.addEintrag(MenueEintrag('4', "Spielfeld bearbeiten",          menu_spielfeld));
  m1.addEintrag(MenueEintrag('5', "Regeln bearbeiten",             menu_regeln));
  m1.addEintrag(MenueEintrag('0', "Programm beenden",              menu_abbrechen));

  // hauptmenü anzeigen, bis beendet gewählt wurde
  while (m1.anzeigen());

  // spiel aufräumen
  exit_game();

  cout << "Programm beendet." << endl;
  return 0;
}