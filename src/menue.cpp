#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include "debug.h"
#ifdef DEBUG
extern std::ofstream s_logfile;
#endif
DEBUG_UNIT(s_logfile)

#include "menue.h"

#include <limits>

using namespace std;



MenueEintrag::MenueEintrag(char kuerzel, const std::string& titel, handle_menue_eintrag_t handler):
  m_kuerzel(kuerzel),
  m_titel(titel),
  m_handler(handler)
{
  DEBUG_FUNCTION()

  //ctor
}

MenueEintrag::~MenueEintrag()
{
  DEBUG_FUNCTION()

  //dtor
}

MenueEintrag::MenueEintrag(const MenueEintrag& other):
  m_kuerzel(other.m_kuerzel),
  m_titel(other.m_titel),
  m_handler(other.m_handler)
{
  DEBUG_FUNCTION()

  //copy ctor
}

MenueEintrag& MenueEintrag::operator=(const MenueEintrag& other)
{
  DEBUG_FUNCTION()

  // handle self assignment
  if (this == &other)
    return *this;

  //assignment operator
  m_kuerzel = other.m_kuerzel;
  m_titel   = other.m_titel;
  m_handler = other.m_handler;

  return *this;
}

bool MenueEintrag::operator () () const
{
  DEBUG_FUNCTION()

  return m_handler(*this);
}



Menue::Menue(const string& titel, const string& aufforderung, const string& fehlermeldung, istream& in, ostream& out):
  m_eintraege(),
  m_titel(titel),
  m_aufforderung(aufforderung),
  m_fehlermeldung(fehlermeldung),
  m_in(&in),
  m_out(&out)
{
  DEBUG_FUNCTION()

  //ctor
}

Menue::~Menue()
{
  DEBUG_FUNCTION()

  //dtor
}

Menue::Menue(const Menue& other):
  m_eintraege(other.m_eintraege),
  m_titel(other.m_titel),
  m_aufforderung(other.m_aufforderung),
  m_fehlermeldung(other.m_fehlermeldung),
  m_in(other.m_in),
  m_out(other.m_out)
{
  DEBUG_FUNCTION()

  //copy ctor
}

Menue& Menue::operator=(const Menue& other)
{
  DEBUG_FUNCTION()

  // handle self assignment
  if (this == &other)
    return *this;

  //assignment operator
  m_eintraege    = other.m_eintraege;
  m_titel        = other.m_titel;
  m_aufforderung = other.m_aufforderung;
  m_fehlermeldung = other.m_fehlermeldung,
  m_in  = const_cast<istream*>(other.m_in);
  m_out = const_cast<ostream*>(other.m_out);

  return *this;
}

void Menue::addEintrag(const MenueEintrag& eintrag)
{
  DEBUG_FUNCTION()

  m_eintraege.push_back(eintrag);
}

bool Menue::anzeigen() const
{
  DEBUG_FUNCTION()

  *m_out << m_titel << endl << endl;

  eintraege_t::const_iterator it = m_eintraege.begin();
  eintraege_t::const_iterator end = m_eintraege.end();

  for (; it != end; it++)
  {
    *m_out << "\t" << it->getKuerzel() << "\t" << it->getTitel() << endl;
  }

  *m_out << endl << m_aufforderung << " ";

  char eingabe;
  *m_in >> eingabe;
  m_in->ignore(numeric_limits<streamsize>::max(), '\n');

  for (it = m_eintraege.begin(); it != end; it++)
  {
    if (it->getKuerzel() == eingabe)
    {
      return (*it)();
    }
  }

  *m_out << endl << m_fehlermeldung << endl;
  return true;
}
