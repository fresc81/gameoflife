#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include "debug.h"
#ifdef DEBUG
extern std::ofstream s_logfile;
#endif
DEBUG_UNIT(s_logfile)

#include "game.h"
#include "console.h"

#include <unistd.h>
#include <signal.h>

#define GAME_DELAY 10000
#define ANZAHL_HISTORIE 16

using namespace std;
using namespace console;

static bool s_running = false;  // simulation läuft
static bool s_quit = false;     // ctrl-c gedrückt
static bool s_resize = false;   // terminal größe geändert

static unsigned short s_width;  // terminal breite
static unsigned short s_height; // terminal höhe

static string s_hline;          // horizontale linie aus = zeichen

// ctrl-c behandeln
static void intproc(int)
{
  /* NOTE some versions of UNIX will reset signal to default
  after each call. So for portability reset signal each time */
  signal(SIGINT, intproc);

  DEBUG_FUNCTION()

  s_quit = true;
}

// terminalgröße geändert behandeln
static void winchproc(int)
{
  signal(SIGWINCH, winchproc);

  DEBUG_FUNCTION()

  s_resize = true;
}

GameOfLife::GameOfLife():
  m_spielfeld(),
  m_regeln(),
  m_aussehen(5),
  m_historie()
{
  DEBUG_FUNCTION()

  m_aussehen[ZELLE_TOT ] = escape("37m")+"."+escape("m");
  m_aussehen[ZELLE_TYPA] = escape("34m")+"#"+escape("m");
  m_aussehen[ZELLE_TYPB] = escape("31m")+"#"+escape("m");
  m_aussehen[ZELLE_TYPC] = escape("36m")+"#"+escape("m");
  m_aussehen[ZELLE_TYPD] = escape("32m")+"#"+escape("m");

  m_historie.push_back(m_spielfeld);
}

GameOfLife::GameOfLife(const Spielfeld& spielfeld, const Regeln& regeln, const zelle_aussehen_t& aussehen):
  m_spielfeld(spielfeld),
  m_regeln(regeln),
  m_aussehen(aussehen),
  m_historie()
{
  DEBUG_FUNCTION()

  m_historie.push_back(spielfeld);
}

GameOfLife::GameOfLife(const GameOfLife& other):
  m_spielfeld(other.m_spielfeld),
  m_regeln(other.m_regeln),
  m_aussehen(other.m_aussehen),
  m_historie(other.m_historie)
{
  DEBUG_FUNCTION()

}

GameOfLife::~GameOfLife()
{
  DEBUG_FUNCTION()

}

GameOfLife& GameOfLife::operator = (const GameOfLife& other)
{
  DEBUG_FUNCTION()

  if (this == &other)
    return *this;

  m_spielfeld = other.m_spielfeld;
  m_regeln = other.m_regeln;
  m_aussehen = other.m_aussehen;
  m_historie = other.m_historie;

  return *this;
}

void GameOfLife::nextSpielzug()
{
  DEBUG_FUNCTION()

  m_spielfeld = m_regeln.anwenden(m_spielfeld);
  m_historie.push_back(m_spielfeld);

  while (m_historie.size() > ANZAHL_HISTORIE)
    m_historie.pop_front();

}

bool GameOfLife::hasKeineAenderungMehr()
{
  DEBUG_FUNCTION()

  if (m_historie.size() > 1)
  {
    spielfeld_queue_t::iterator it = m_historie.begin();

    Spielfeld& s0 = *it;
    ++it;

    for (size_t i = 1; i < m_historie.size(); i++)
    {
      Spielfeld& s1 = *it;

      if (s0 == s1)
        return true;

      ++it;
    }

  }

  return false;
}

SimulationsErgebnis GameOfLife::simulationStarten(GameOfLife& game)
{
  DEBUG_FUNCTION()

  if (s_running)
    return SIMULATION_BEREITS_GESTARTET;

  s_running = true;

  s_quit = false;
  s_resize = false;

  SimulationsErgebnis result = SIMULATION_OKAY;

  game.m_historie.clear();
  game.m_historie.push_back(game.m_spielfeld);

  clear();
  if (!checkConsoleSize(game.m_spielfeld)) {
    s_running = false;
    return SIMULATION_KONSOLE_ZU_KLEIN;
  }

  signal(SIGINT, intproc);
  signal(SIGWINCH, winchproc);

  for (size_t i = 0;;i++)
  {
    if (s_quit)
    {
      s_quit = false;
      break;
    }

    if (s_resize)
    {
      s_resize = false;
      if (!checkConsoleSize(game.m_spielfeld))
      {
        result = SIMULATION_KONSOLE_ZU_KLEIN;
        break;
      }
      clear(false);
    }

    cursorxy();
    cout << escape("33m") << "Generation: " << i << " [" << s_width << "x" << s_height << "]" << escape("m") << '\n';
    cout << s_hline << '\n';
    cout << game;
    cout << s_hline << '\n';
    cout << escape("33m") << "Anhalten mit CTRL-C" << escape("m") << endl;

    game.nextSpielzug();

    if (game.hasKeineAenderungMehr())
    {
      result = SIMULATION_KEINE_AENDERUNG_MEHR;
      break;
    }

    usleep(GAME_DELAY);
  }

  signal(SIGINT, NULL);
  signal(SIGWINCH, NULL);

  s_running = false;
  return result;
}

bool GameOfLife::checkConsoleSize(const Spielfeld& spielfeld)
{
  DEBUG_FUNCTION()

  console::size(s_width, s_height);

  s_hline = "";
  for (size_t i=0; i<s_width; i++)
    s_hline.append( "=" );

  return ((int)spielfeld.getN() <= s_height - 6) && ((int)spielfeld.getM() <= s_width);
}

ostream& operator << (ostream& out, const GameOfLife& game)
{
  DEBUG_FUNCTION()

  const Spielfeld& spielfeld = game.getSpielfeld();
  const GameOfLife::zelle_aussehen_t& aussehen = game.getAussehen();

  const size_t n = spielfeld.getN();
  const size_t m = spielfeld.getM();

  zelle_zustand zustand;
  for (size_t i = 0; i < n; i++)
  {
    for (size_t j = 0; j < m; j++)
    {
      zustand = spielfeld(i, j);
      out << aussehen[zustand];
    }
    out << '\n';
  }
  out << flush;
  return out;
}

/*
istream& operator >> (istream& in, GameOfLife& game)
{

  // TODO implement

  return in;
}
*/