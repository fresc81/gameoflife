#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include "debug.h"
#ifdef DEBUG
extern std::ofstream s_logfile;
#endif
DEBUG_UNIT(s_logfile)

#include "console.h"

#include <sys/ioctl.h>

#include <unistd.h>

#ifdef HAVE_TERM_H
#	include <term.h>
#endif

void console::size(unsigned short& width, unsigned short& height)
{
  DEBUG_FUNCTION()

  struct winsize w;
  ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

  height = w.ws_row;
  width = w.ws_col;
}